FROM docker.io/mattdark/rust-python:latest

WORKDIR /home/admin
USER admin
RUN mkdir app
WORKDIR app

RUN env PYTHON_CONFIGURE_OPTS='--enable-shared' pyenv install 3.7.7
ENV LD_LIBRARY_PATH $HOME/.pyenv/versions/3.7.7/lib/

RUN pyenv global 3.7.7
RUN pyenv rehash

RUN rustup override set nightly

COPY pyproject.toml ./

RUN poetry config virtualenvs.create false \
    && poetry lock \
    && poetry export --without-hashes -f requirements.txt --dev \
    |  poetry run pip install -r /dev/stdin \
    && poetry debug

COPY  . ./

RUN poetry install --no-interaction \
    && cargo build --release

CMD ["sh", "run"]