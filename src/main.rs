#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

extern crate rocket_contrib;
extern crate cpython;

use cpython::{Python, PyResult, PyModule};

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::collections::HashMap;
use std::path::{Path, PathBuf};
use crate::handlebars::{to_json};

use rocket::response::NamedFile;
use rocket_contrib::templates::{Template, handlebars};


#[derive(Serialize, Deserialize, Debug)]
pub struct Speakers {
    pub id: String,
    pub name: String,
    pub last_name: String,
}

const FIRE_PY: &'static str = include_str!("./python/pyrebase.py");

#[get("/")]
fn index() -> Template {
    let gil = Python::acquire_gil();
    let py = gil.python();
    let s = example(py).unwrap();
    let mut data = HashMap::new();
    data.insert("speakers".to_string(), to_json(&s));
    Template::render("index", &data)
}

fn example(py: Python<'_>) -> PyResult<Vec<Speakers>> {
    let m = module_from_str(py, "pyrebase", FIRE_PY)?;

    let out: String = m.call(py, "read_data", (2,), None)?.extract(py)?;

    let speakers: Vec<Speakers> = serde_json::from_str(&out).unwrap();
    
    Ok(speakers)
}

/// Import a module from the given file contents.
///
/// This is a wrapper around `PyModule::new` and `Python::run` which simulates
/// the behavior of the builtin function `exec`. `name` will be used as the
/// module's `__name__`, but is not otherwise important (it does not need
/// to match the file's name).
///
/// Note this compiles and executes the module code each time it is called, as it
/// bypasses the regular import mechanism. No entry is added to the cache in `sys.modules`.
fn module_from_str(py: Python<'_>, name: &str, source: &str) -> PyResult<PyModule> {
    let m = PyModule::new(py, name)?;
    m.add(py, "__builtins__", py.import("builtins")?)?;

    let m_locals = m.get(py, "__dict__")?.extract(py)?;
    py.run(source, Some(&m_locals), None)?;
    Ok(m)
}

#[get("/<file..>", rank=3)]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount("/", routes![index, files])
    .attach(Template::fairing())
}

fn main() {
    rocket().launch();
}
