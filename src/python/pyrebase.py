import json
from firebase import Firebase
def read_data(self):
    config = {
        "apiKey": "APIKEY",
        "authDomain": "rust-python-demo.firebaseapp.com",
        "databaseURL": "https://rust-python-demo.firebaseio.com",
        "projectId": "rust-python-demo",
        "storageBucket": "rust-python-demo.appspot.com",
        "messagingSenderId": "MESSAGINGSENDERID"
    }
    firebase = Firebase(config)

    speaker = list()
    db = firebase.database()
    all_speakers = db.child("speakers").get()
    for x in all_speakers.each():
        speaker.append(x.val())
    s = json.dumps(speaker)
    return s
